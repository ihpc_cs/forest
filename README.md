# Backup for Kaggle Competition "Planet: Understanding the Amazon from Space | Kaggle"
# Thank Heng CherKeng for Sharing his Pytorch Source Codes, greatly appreciated it. 

#in order to run the codes, you have to do the follows:
#1.put train and test set in './input', put train_v2.csv, valid_v2.csv (dummy file), test_v2.csv in the folder of './input'
#2.download the pretrained CNN weights and put in './models'
#3.turn on "do_training", then run CUDA_VISIBLE_DEVICES=x python train-forest.py --cv_fold_num x (x denotes 0,1,2,3) to train a model for x folder (4-folder cross validation)
#4.you may turn on "do_csv_train" or "do_csv_test" to generate the probability values in terms of csv files for train set (oof from 4 models) and test set (average from 4 models)
#5.you may turn on "do_threshold" and "do_submission" to get the optimized threshold values and its corresponding submission for one specific model
#6.you may trun on "do_threshold_ensemble" and "do_threshold_ensemble" to get the optimized threshold values and its corresonding submission for the ensemble of various modles. 

#any issues please contact yangx@ihpc.a-star.edu.sg
