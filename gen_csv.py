
## using imagenet standard nets like vgg, resnet, resenxt, densenet, inception

## https://github.com/colesbury/examples/blob/8a19c609a43dc7a74d1d4c71efcda102eed59365/imagenet/main.py
## https://github.com/pytorch/examples/blob/master/imagenet/main.py


from net.common import *
from net.dataset.tool import *
from net.utility.tool import *
from net.rates import *

from net.dataset.kgforest import *
from sklearn.metrics import fbeta_score

################### input option no.1 ##############################
###################select one of the following net models
#--------------------------------------------
#from net.model.resnet import resnet18 as Net
#from net.model.resnet import resnet34 as Net
from net.model.resnet import resnet50 as Net
#from net.model.resnet import resnet101 as Net
#from net.model.resnet import resnet152 as Net

#from net.model.densenet import densenet121 as Net
#from net.model.densenet import densenet161 as Net
#from net.model.densenet import densenet169 as Net

#from net.model.inceptionv3 import Inception3 as Net
#from net.model.inceptionv4 import Inception4 as Net

#from net.model.vggnet import vgg16 as Net
#from net.model.vggnet import vgg19 as Net


## global setting ################
SIZE =  256 #256   #128  #112


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--cv_fold_num', default = 1, help='select cv folder')

opt = parser.parse_args()
print opt


fold_num = int(opt.cv_fold_num)


##--- helpe functions  -------------
def change_images(images, agument):

    num = len(images)
    if agument == 'left-right' :
        for n in range(num):
            image = images[n]
            images[n] = cv2.flip(image,1)

    if agument == 'up-down' :
        for n in range(num):
            image = images[n]
            images[n] = cv2.flip(image,0)

    # if agument == 'rotate':
    #     for n in range(num):
    #         image = images[n]
    #         images[n] = randomRotate90(image)  ##randomRotate90  ##randomRotate
    #

    if agument == 'transpose' :
        for n in range(num):
            image = images[n]
            images[n] = image.transpose(1,0,2)


    if agument == 'rotate90' :
        for n in range(num):
            image = images[n]
            image = image.transpose(1,0,2)  #cv2.transpose(img)
            images[n]  = cv2.flip(image,1)


    if agument == 'rotate180' :
        for n in range(num):
            image = images[n]
            images[n] = cv2.flip(image,-1)


    if agument == 'rotate270' :
        for n in range(num):
            image = images[n]
            image = image.transpose(1,0,2)  #cv2.transpose(img)
            images[n]  = cv2.flip(image,0)

    return images


#------------------------------------------------------------------------------------------------------------
#https://www.kaggle.com/paulorzp/planet-understanding-the-amazon-from-space/find-best-f2-score-threshold/code
def find_f_measure_threshold1(probs, labels, thresholds=None):

    #f0 = fbeta_score(labels, probs, beta=2, average='samples')  #micro  #samples
    def _f_measure(probs, labels, threshold=0.5, beta=2 ):

        SMALL = 1e-12 #0  #1e-12
        batch_size, num_classes = labels.shape[0:2]

        l = labels
        p = probs>threshold

        num_pos     = p.sum(axis=1) + SMALL
        num_pos_hat = l.sum(axis=1)
        tp          = (l*p).sum(axis=1)
        precise     = tp/num_pos
        recall      = tp/num_pos_hat

        fs = (1+beta*beta)*precise*recall/(beta*beta*precise + recall + SMALL)
        f  = fs.sum()/batch_size
        return f


    best_threshold =  0
    best_score     = -1

    if thresholds is None:
        thresholds = np.arange(0,1,0.005)
        ##thresholds = np.unique(probs)

    N=len(thresholds)
    scores = np.zeros(N,np.float32)
    for n in range(N):
        t = thresholds[n]
        #score = f_measure(probs, labels, threshold=t)
        score = fbeta_score(labels, probs>t, beta=2, average='samples')  #micro  #samples
        scores[n] = score

    return thresholds, scores



def find_f_measure_threshold2(probs, labels, num_iters=100, seed=0.235):

    batch_size, num_classes = labels.shape[0:2]

    best_thresholds = [seed]*num_classes
    best_scores     = [0]*num_classes
    for t in range(num_classes):

        thresholds = [seed]*num_classes
        for i in range(num_iters):
            th = i / float(num_iters)
            thresholds[t] = th
            f2 = fbeta_score(labels, probs > thresholds, beta=2, average='samples')
            if  f2 > best_scores[t]:
                best_scores[t]     = f2
                best_thresholds[t] = th
        print('\t(t, best_thresholds[t], best_scores[t])=%2d, %0.3f, %f'%(t, best_thresholds[t], best_scores[t]))
    print('')
    return best_thresholds, best_scores


from scipy.optimize import minimize
from scipy.spatial import distance

def f2_score(y_true, y_pred):
    y_true, y_pred, = np.array(y_true), np.array(y_pred)
    return fbeta_score(y_true, y_pred, beta=2, average='samples') 


def find_f_measure_threshold2_optim(probs, labels):
    loss = lambda thres: -f2_score(labels, probs > thres)  + distance.euclidean(thres, np.clip(thres, 0, 1))
    initial_thresholds = [0.2] * 17
    # Run Nelder-Mead before Powell
    min_result = minimize(loss, initial_thresholds, method='Nelder-Mead')
    min_result = minimize(loss, min_result.x, method='Powell')
    # Run only Powell
    min_result2 = minimize(loss, initial_thresholds, method='Powell')
    # Find best result
    if min_result.fun < min_result2.fun:
        rtn = min_result.x
    else:
        rtn = min_result2.x
    return rtn


#------------------------------------------------------------------------------------------------------------


#precision_recall_curve
def binary_precision_recall_curve(labels, predictions, beta=2):

    precise, recall, threshold = sklearn.metrics.precision_recall_curve(labels, predictions)
    f2 = (1+beta*beta)*precise*recall/(beta*beta*precise + recall + 1e-12)  #beta=2  #f2 score
    idx = np.argmax(f2)

    return precise, recall, f2, threshold, idx



# write csv
def write_submission_csv(csv_file, predictions, thresholds):

    class_names = CLASS_NAMES
    num_classes = len(class_names)

    with open(KAGGLE_DATA_DIR+'/split/test-61191') as f:
        names = f.readlines()
    names = [x.strip() for x in names]
    num_test = len(names)


    assert((num_test,num_classes) == predictions.shape)
    with open(csv_file,'w') as f:
        f.write('image_name,tags\n')
        for n in range(num_test):
            shortname = names[n].split('/')[-1].replace('.<ext>','')

            prediction = predictions[n]
            s = score_to_class_names(prediction, class_names, threshold=thresholds)
            f.write('%s,%s\n'%(shortname,s))



# loss ----------------------------------------
def multi_criterion(logits, labels):
    loss = nn.MultiLabelSoftMarginLoss()(logits, Variable(labels))
    return loss

#https://www.kaggle.com/paulorzp/planet-understanding-the-amazon-from-space/find-best-f2-score-threshold/code
#f  = fbeta_score(labels, probs, beta=2, average='samples')
def multi_f_measure( probs, labels, threshold=0.235, beta=2 ):

    SMALL = 1e-6 #0  #1e-12
    batch_size = probs.size()[0]

    #weather
    l = labels
    p = (probs>threshold).float()

    num_pos     = torch.sum(p,  1)
    num_pos_hat = torch.sum(l,  1)
    tp          = torch.sum(l*p,1)
    precise     = tp/(num_pos     + SMALL)
    recall      = tp/(num_pos_hat + SMALL)

    fs = (1+beta*beta)*precise*recall/(beta*beta*precise + recall + SMALL)
    f  = fs.sum()/batch_size
    return f



## main functions ##
def predict(net, test_loader):

    test_dataset = test_loader.dataset
    num_classes  = len(test_dataset.class_names)
    predictions  = np.zeros((test_dataset.num,num_classes),np.float32)

    test_num  = 0
    for iter, (images, indices) in enumerate(test_loader, 0):

        # forward
        logits, probs = net(Variable(images.cuda(),volatile=True))

        batch_size = len(images)
        test_num  += batch_size
        start = test_num-batch_size
        end   = test_num
        predictions[start:end] = probs.data.cpu().numpy().reshape(-1,num_classes)

    assert(test_dataset.num==test_num)

    return predictions



def evaluate( net, test_loader ):

    test_num  = 0
    test_loss = 0
    test_acc  = 0
    for iter, (images, labels, indices) in enumerate(test_loader, 0):

        # forward
        logits, probs = net(Variable(images.cuda(),volatile=True))
        loss  = multi_criterion(logits, labels.cuda())

        batch_size = len(images)
        test_acc  += batch_size*multi_f_measure(probs.data, labels.cuda())
        test_loss += batch_size*loss.data[0]
        test_num  += batch_size

    assert(test_num == test_loader.dataset.num)
    test_acc  = test_acc/test_num
    test_loss = test_loss/test_num

    return test_loss, test_acc



def evaluate_and_predict(net, test_loader):

    test_dataset = test_loader.dataset
    num_classes  = len(test_dataset.class_names)
    predictions  = np.zeros((test_dataset.num,num_classes),np.float32)

    test_num  = 0
    test_loss = 0
    test_acc  = 0
    for iter, (images, labels, indices) in enumerate(test_loader, 0):

        # forward
        logits, probs = net(Variable(images.cuda(),volatile=True))
        loss  = multi_criterion(logits, labels.cuda())

        batch_size = len(images)
        test_acc  += batch_size*multi_f_measure(probs.data, labels.cuda())
        test_loss += batch_size*loss.data[0]
        test_num  += batch_size

        start = test_num-batch_size
        end   = test_num
        predictions[start:end] = probs.data.cpu().numpy().reshape(-1,num_classes)

    assert(test_dataset.num==test_num)
    test_acc  = test_acc/test_num
    test_loss = test_loss/test_num

    return test_loss, test_acc, predictions




def do_training():

    out_dir =os.path.join('./output','model'+str(fold_num))
    initial_checkpoint = None 
    initial_model      = None


##################### input option no.2 #################################
##################### download and select the weights corresponding to the models selected in option no.1


    #pretrained_file = './models/resnet18-5c106cde.pth'
    #pretrained_file = './models/resnet34-333f7ec4.pth'
    pretrained_file = './models/resnet50-19c8e357.pth'
    #pretrained_file = './models/resnet101-5d3b4d8f.pth'
    #pretrained_file = './models/resnet152-b121ed2d.pth'

    #pretrained_file = './models/densenet121-241335ed.pth' 
    #pretrained_file = './models/densenet169-6f0f7f60.pth'
    #pretrained_file = './models/densenet201-4c113574.pth'

    #pretrained_file = './models/inception_v3_google-1a9a5a14.pth'
    #pretrained_file = './models/inceptionv4-58153ba9.pth'


    #pretrained_file = './models/vgg16-397923af.pth'
    #pretrained_file = './models/vgg19-dcbb9e9d.pth'



    ## ------------------------------------

    try:
        os.stat(out_dir +'/snap')
    except:
        os.mkdir(out_dir +'/snap')

    try:
        os.stat(out_dir +'/checkpoint')
    except:
        os.mkdir(out_dir +'/checkpoint')


    log = Logger()
    log.open(out_dir+'/log.train.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('** some experiment setting **\n')
    log.write('\tSEED    = %u\n' % SEED)
    log.write('\tfile    = %s\n' % __file__)
    log.write('\tout_dir = %s\n' % out_dir)
    log.write('\n')


    ## dataset ----------------------------------------
    log.write('** dataset setting **\n')
    num_classes = len(CLASS_NAMES)

    batch_size  = 48 # reduce this size if out of memory

    train_dataset = KgForestDataset('train',
                                    transform=transforms.Compose([
                                        #transforms.Lambda(lambda x: randomRotate(x, u=0.5, limit=45)),
                                        #transforms.Lambda(lambda x: randomShiftScale (x, u=0.5, limit=5)),
                                        transforms.Lambda(lambda x: randomShiftScaleRotate(x, u=0.5, shift_limit=4, scale_limit=4, rotate_limit=45)),
                                        transforms.Lambda(lambda x: randomFlip(x)),
                                        transforms.Lambda(lambda x: randomTranspose(x)),
                                        transforms.Lambda(lambda x: img_to_tensor(x)),
                                    ]),
                                    height=SIZE,width=SIZE,
                                    label_csv='train_'+str(fold_num)+'.csv')
    train_loader  = DataLoader(
                        train_dataset,
                        sampler = RandomSampler(train_dataset), 
                        batch_size  = batch_size,
                        drop_last   = True,
                        num_workers = 3,
                        pin_memory  = True)

    

    test_dataset = KgForestDataset('valid', 
                                    transform=transforms.Compose([
                                        transforms.Lambda(lambda x: img_to_tensor(x)),
                                    ]),
                                    height=SIZE,width=SIZE,
                                    label_csv='valid_'+str(fold_num)+'.csv')
    test_loader  = DataLoader(
                        test_dataset,
                        sampler     = SequentialSampler(test_dataset),  
                        batch_size  = batch_size,
                        drop_last   = False,
                        num_workers = 3,
                        pin_memory  = True)


    height, width , in_channels   = test_dataset.images[0].shape
    log.write('\t(height,width)    = (%d, %d)\n'%(height,width))
    log.write('\tin_channels       = %d\n'%(in_channels))
    log.write('\ttrain_dataset.split  = %s\n'%(train_dataset.split))
    log.write('\ttrain_dataset.num    = %d\n'%(train_dataset.num))
    log.write('\ttest_dataset.split   = %s\n'%(test_dataset.split))
    log.write('\ttest_dataset.num     = %d\n'%(test_dataset.num))
    log.write('\tbatch_size           = %d\n'%batch_size)
    log.write('\ttrain_loader.sampler = %s\n'%(str(train_loader.sampler)))
    log.write('\ttest_loader.sampler  = %s\n'%(str(test_loader.sampler)))
    log.write('\n')



    ## net ----------------------------------------
    log.write('** net setting **\n')
    net = Net(in_shape = (in_channels, height, width), num_classes=num_classes)

    net.cuda()
    #log.write('\n%s\n'%(str(net)))
    log.write('%s\n\n'%(type(net)))
    log.write(inspect.getsource(net.__init__)+'\n')
    log.write(inspect.getsource(net.forward)+'\n')
    log.write('\n')


    #resume from previous?
    start_epoch=0
    if initial_checkpoint is not None:
        checkpoint  = torch.load(initial_checkpoint)

        start_epoch = checkpoint['epoch']  #start_epoch = int(initial_checkpoint.split('/')[-1].replace('.pth',''))
        optimizer.load_state_dict(checkpoint['optimizer'])
        pretrained_dict = checkpoint['state_dict']
        load_valid(net, pretrained_dict)
        del pretrained_dict

    if pretrained_file is not None:  #pretrain
        skip_list = ['fc.weight', 'fc.bias']
        #skip_list = ['fc.0.weight', 'fc.0.bias', 'fc.3.weight', 'fc.3.bias', 'fc.6.weight', 'fc.6.bias'] # #for vgg16
        load_valid(net, pretrained_file, skip_list=skip_list)

    if initial_model is not None:
       net = torch.load(initial_model)


    ## fine tunning
    LR = StepLR(steps=[0,    10,    20,    35,    40,], \
                rates=[0.01, 0.005, 0.001, 0.0001, -1])


    num_epoches = 50  
    it_print    = 20 
    epoch_test  = 1
    epoch_save  = 5

    #optimizer = optim.Adam(net.parameters(), lr=0.001, weight_decay=0.001)
    optimizer = optim.SGD(net.parameters(), lr=0.1, momentum=0.9, weight_decay=0.0005)  ###0.0005

    #only fc
    #optimizer = optim.SGD(net.fc.parameters(), lr=0.1, momentum=0.9, weight_decay=0.0005)

    #print some params for debug
    ## print(net.features.state_dict()['0.conv.weight'][0,0])



    ## start training here! ##############################################3
    log.write('** start training here! **\n')

    log.write(' optimizer=%s\n'%str(optimizer) )
    log.write(' LR=%s\n\n'%str(LR) )
    log.write(' epoch   iter   rate  |  smooth_loss   |  train_loss  (acc)  |  valid_loss  (acc)  | min\n')
    log.write('----------------------------------------------------------------------------------------\n')

    smooth_loss = 0.0
    train_loss  = np.nan
    train_acc   = np.nan
    test_loss   = np.nan
    test_acc    = np.nan
    time = 0

    start0 = timer()
    for epoch in range(start_epoch, num_epoches):  # loop over the dataset multiple times
        #print ('epoch=%d'%epoch)
        start = timer()

        #---learning rate schduler ------------------------------
        lr =  LR.get_rate(epoch, num_epoches)
        if lr<0 :break

        adjust_learning_rate(optimizer, lr)
        rate =  get_learning_rate(optimizer)[0] #check
        #--------------------------------------------------------

        sum_smooth_loss = 0.0
        sum = 0
        net.cuda().train()
        num_its = len(train_loader)
        for it, (images, labels, indices) in enumerate(train_loader, 0):

            logits, probs = net(Variable(images.cuda()))
            loss  = multi_criterion(logits, labels.cuda())

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            #additional metrics
            sum_smooth_loss += loss.data[0]
            sum += 1

            #print some params for debug
            ##print(net.features.state_dict()['0.conv.weight'][0,0])

            # print statistics
            if it % it_print == it_print-1:
                smooth_loss = sum_smooth_loss/sum
                sum_smooth_loss = 0.0
                sum = 0

                train_acc  = multi_f_measure(probs.data, labels.cuda())
                train_loss = loss.data[0]

                #print('\r%5.1f   %5d    %0.4f   |  %0.3f  | %0.3f  %5.3f | ... ' % \
                #        (epoch + it/num_its, it + 1, rate, smooth_loss, train_loss, train_acc),\
                #        end='',flush=True)


        #---- end of one epoch -----
        end = timer()
        time = (end - start)/60

        if epoch % epoch_test == epoch_test-1  or epoch == num_epoches-1:

            net.cuda().eval()
            test_loss,test_acc = evaluate(net, test_loader)

            #print('\r',end='',flush=True)
            log.write('%5.1f   %5d    %0.4f   |  %0.3f  | %0.3f  %5.3f | %0.3f  %5.3f  |  %3.1f min \n' % \
                    (epoch + 1, it + 1, rate, smooth_loss, train_loss, train_acc, test_loss,test_acc, time))

        if epoch % epoch_save == epoch_save-1 or epoch == num_epoches-1:
            torch.save(net, out_dir +'/snap/%03d.torch'%(epoch+1))
            torch.save({
                'state_dict': net.state_dict(),
                'optimizer' : optimizer.state_dict(),
                'epoch'     : epoch,
            }, out_dir +'/checkpoint/%03d.pth'%(epoch+1))
            ## https://github.com/pytorch/examples/blob/master/imagenet/main.py




    #---- end of all epoches -----
    end0  = timer()
    time0 = (end0 - start0) / 60

    ## check : load model and re-test
    torch.save(net,out_dir +'/snap/final.torch')
    if 1:
        net = torch.load(out_dir +'/snap/final.torch')

        net.cuda().eval()
        test_loss, test_acc, predictions = evaluate_and_predict( net, test_loader )

        log.write('\n')
        log.write('%s:\n'%(out_dir +'/snap/final.torch'))
        log.write('\tall time to train=%0.1f min\n'%(time0))
        log.write('\ttest_loss=%f, test_acc=%f\n'%(test_loss,test_acc))

        #assert(type(test_loader.sampler)==torch.utils.data.sampler.SequentialSampler)
        #dump_results(out_dir +'/train' , predictions, test_dataset.labels, test_dataset.names)




##to determine best threshold etc ... ## -----------------------------------------------------------

def do_find_thresholds():

    out_dir = './output'
    model_file = out_dir +'/snap/final.torch'  


    ## ------------------------------------
    log = Logger()
    log.open(out_dir+'/log.thresholds.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('\n')

    ## dataset ----------------------------
    log.write('** dataset setting **\n')
    batch_size = 48

    test_dataset = KgForestDataset('train-40479',  #'valid-8000',   #'train-simple-road-280',
                                    transform=transforms.Compose([
                                        transforms.Lambda(lambda x: img_to_tensor(x)),
                                    ]),
                                    height=SIZE,width=SIZE,
                                    label_csv='train_v2.csv')
    test_loader  = DataLoader(
                        test_dataset,
                        sampler     = SequentialSampler(test_dataset),  #None,
                        batch_size  = batch_size,
                        drop_last   = False,
                        num_workers = 4,
                        pin_memory  = True)

    height, width , in_channels   = test_dataset.images[0].shape
    log.write('\t(height,width)     = (%d, %d)\n'%(height,width))
    log.write('\tin_channels        = %d\n'%(in_channels))
    log.write('\ttest_dataset.split = %s\n'%(test_dataset.split))
    log.write('\ttest_dataset.num   = %d\n'%(test_dataset.num))
    log.write('\tbatch_size         = %d\n'%batch_size)
    log.write('\n')


    ## net ----------------------------------------
    log.write('** net setting **\n')
    log.write('\tmodel_file = %s\n'%model_file)
    log.write('\n')

    net = torch.load(model_file)
    net.cuda().eval()


    # do testing here ###
    aguments = ['default', 'left-right', 'up-down', 'transpose',
                'rotate90', 'rotate180', 'rotate270', ]
    #aguments = ['default', 'left-right']
    num_augments=len(aguments)
    num_classes  = len(test_dataset.class_names)
    test_num     = test_dataset.num
    test_dataset_images = test_dataset.images.copy()

    all_predictions = np.zeros((num_augments+1, test_num, num_classes),np.float32)
    for a in range(num_augments):
        agument = aguments[a]
        log.write('** predict @ agument = %s **\n'%agument)

        test_dataset.images = change_images(test_dataset_images,agument)
        test_loss, test_acc, predictions = evaluate_and_predict( net, test_loader )
        all_predictions[a] = predictions

        log.write('\t\ttest_loss=%f, test_acc=%f\n\n'%(test_loss,test_acc))


    # add average case ...
    aguments = aguments + ['average']
    predictions = all_predictions.sum(axis=0)/num_augments
    all_predictions[num_augments] = predictions
    log.write('\n')

    try:
        os.stat(out_dir +'/thresholds/')
    except:
        os.mkdir(out_dir +'/thresholds/')


    # find thresholds and save all
    labels = test_dataset.labels
    for a in range(num_augments+1):
        agument = aguments[a]
        predictions = all_predictions[a]

        #test_dir = out_dir +'/thresholds/'+ agument
        #os.makedirs(test_dir, exist_ok=True)

        test_dir = out_dir +'/thresholds/'+ agument

        try:
            os.stat(test_dir)
        except:
            os.mkdir(test_dir)


        log.write('** thresholding @ test_dir = %s **\n'%test_dir)

        #find threshold --------------------------
        if 1:
            thresholds, scores = find_f_measure_threshold1(predictions, labels)
            i = np.argmax(scores)
            best_threshold, best_score = thresholds[i], scores[i]

            log.write('\tmethod1:\n')
            log.write('\tbest_threshold=%f, best_score=%f\n\n'%(best_threshold, best_score))
            #plot_f_measure_threshold(thresholds, scores)
            #plt.pause(0)

        if 1:
            seed = best_threshold  #0.21  #best_threshold

            best_thresholds,  best_scores = find_f_measure_threshold2(predictions, labels, num_iters=100, seed=0.21)
            log.write('\tmethod2:\n')
            log.write('\tbest_threshold\n')
            log.write (str(best_thresholds)+'\n')
            log.write('\tbest_scores\n')
            log.write (str(best_scores)+'\n')
            log.write('\n')

        #--------------------------------------------
        assert(type(test_loader.sampler)==torch.utils.data.sampler.SequentialSampler)
        np.save(test_dir +'/predictions.npy',predictions)
        np.save(test_dir +'/labels.npy',labels)
        np.savetxt(test_dir +'/best_threshold.txt', np.array(best_thresholds),fmt='%.5f' )
        np.savetxt(test_dir +'/best_scores.txt',np.array(best_scores),fmt='%.5f')
    # pass


##-----------------------------------------



def do_submissions_test():

    ## dataset ----------------------------
    out_dir_org = './output/resnet34'
    log = Logger()
    log.open(out_dir_org+'/log.submissions_test.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('\n')




    log.write('** dataset setting **\n')
    batch_size    = 48  #128

    test_dataset = KgForestDataset('test-61191',  #'valid-8000',   #'train-simple-road-280',
                                    transform=transforms.Compose([
                                        transforms.Lambda(lambda x: img_to_tensor(x)),
                                    ]),
                                    height=SIZE,width=SIZE,
                                    label_csv='test_v2.csv')
    test_loader  = DataLoader(
                        test_dataset,
                        sampler     = SequentialSampler(test_dataset),  #None,
                        batch_size  = batch_size,
                        drop_last   = False,
                        num_workers = 4,
                        pin_memory  = True)

    height, width , in_channels   = test_dataset.images[0].shape
    log.write('\t(height,width)    = (%d, %d)\n'%(height,width))
    log.write('\tin_channels       = %d\n'%(in_channels))
    log.write('\ttest_dataset.split = %s\n'%(test_dataset.split))
    log.write('\ttest_dataset.num  = %d\n'%(test_dataset.num))
    log.write('\tbatch_size        = %d\n'%batch_size)
    log.write('\n')


    df_train = pd.read_csv(os.path.join(KAGGLE_DATA_DIR,'test_v2.csv'))        
    img_id = df_train['image_name']
    labels = CLASS_NAMES
    num_classes  = len(labels)
    test_num     = len(img_id)

    final_predictions = np.zeros((test_num,num_classes),np.float32)


    nFolds = 4
    for fold_num in range(nFolds):
        out_dir = os.path.join(out_dir_org,'model'+str(fold_num))
        model_file = out_dir +'/snap/final.torch'  

        ## net ----------------------------------------
        log.write('** net setting **\n')
        log.write('\tmodel_file = %s\n'%model_file)
        log.write('\n')

        net = torch.load(model_file)
        net.cuda().eval()


        # do testing here ###
        #aguments = ['default']
        aguments = ['default', 'left-right', 'up-down', 'transpose',
                    'rotate90', 'rotate180', 'rotate270', ]
        num_augments = len(aguments)
        num_classes  = len(test_dataset.class_names)
        labels_map = test_dataset.class_names
        test_num     = test_dataset.num
        test_dataset_images = test_dataset.images.copy()

        all_predictions = np.zeros((num_augments+1,test_num,num_classes),np.float32)
        for a in range(num_augments):
            agument = aguments[a]
            log.write('** predict @ agument = %s **\n'%agument)

            ## perturb here for test argumnetation  ## ----
            test_dataset.images = change_images(test_dataset_images,agument)
            #import pdb; pdb.set_trace()
            predictions = predict( net, test_loader )
            all_predictions[a] = predictions

        # add average case ...
        aguments = aguments + ['average']
        predictions = all_predictions.sum(axis=0)/num_augments
        all_predictions[num_augments] = predictions
        log.write('\n')
 

        try:
            os.stat(out_dir +'/submissions/')
        except:
            os.mkdir(out_dir +'/submissions/')

        # apply thresholds and save all
        for a in range(num_augments+1):

            agument = aguments[a]
            predictions = all_predictions[a]

            test_dir = out_dir +'/submissions/'+ agument
            try:
                os.stat(test_dir)
            except:
                os.mkdir(test_dir)


            assert(type(test_loader.sampler)==torch.utils.data.sampler.SequentialSampler)
            np.save(test_dir +'/predictions_test.npy',predictions)
            if a == num_augments:
                final_predictions = final_predictions + predictions

    final_predictions = final_predictions/nFolds
    info = 'oof_test_7pos'
    create_submission(final_predictions, img_id, labels, info)

    pass


from sklearn.model_selection import KFold
def do_submissions_train():

    ## dataset ----------------------------
    out_dir_org = './output/resnet34'


    log = Logger()
    log.open(out_dir_org+'/log.submissions_train.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('\n')

    n_folds = 4

    df_train = pd.read_csv(os.path.join(KAGGLE_DATA_DIR,'train_v2.csv'))        
    img_id = df_train['image_name']
    labels = CLASS_NAMES
    num_classes  = len(labels)
    test_num     = len(img_id)

    final_predictions = np.zeros((test_num,num_classes),np.float32)
    #import pdb; pdb.set_trace()
    kf = KFold(n_folds, random_state=42)
    for fold_num, (train_index, val_index) in enumerate(kf.split(df_train)):
        out_dir = os.path.join(out_dir_org,'model'+str(fold_num))
        model_file = out_dir +'/snap/final.torch'  

        ## dataset ----------------------------
        log.write('** dataset setting **\n')
        batch_size    = 48  #128

        test_dataset = KgForestDataset('test-61191',  #'valid-8000',   #'train-simple-road-280',
                                        transform=transforms.Compose([
                                            transforms.Lambda(lambda x: img_to_tensor(x)),
                                        ]),
                                        height=SIZE,width=SIZE,
                                        label_csv='valid_'+str(fold_num)+'.csv')
        test_loader  = DataLoader(
                            test_dataset,
                            sampler     = SequentialSampler(test_dataset),  #None,
                            batch_size  = batch_size,
                            drop_last   = False,
                            num_workers = 4,
                            pin_memory  = True)

        height, width , in_channels   = test_dataset.images[0].shape
        log.write('\t(height,width)    = (%d, %d)\n'%(height,width))
        log.write('\tin_channels       = %d\n'%(in_channels))
        log.write('\ttest_dataset.split = %s\n'%(test_dataset.split))
        log.write('\ttest_dataset.num  = %d\n'%(test_dataset.num))
        log.write('\tbatch_size        = %d\n'%batch_size)
        log.write('\n')


        ## net ----------------------------------------
        log.write('** net setting **\n')
        log.write('\tmodel_file = %s\n'%model_file)
        log.write('\n')

        net = torch.load(model_file)
        net.cuda().eval()


        # do testing here ###
        #aguments = ['default']
        aguments = ['default', 'left-right', 'up-down', 'transpose',
                    'rotate90', 'rotate180', 'rotate270', ]
        num_augments = len(aguments)
        num_classes  = len(test_dataset.class_names)
        labels_map = test_dataset.class_names
        test_num     = test_dataset.num
        test_dataset_images = test_dataset.images.copy()

        all_predictions = np.zeros((num_augments+1,test_num,num_classes),np.float32)
        for a in range(num_augments):
            agument = aguments[a]
            log.write('** predict @ agument = %s **\n'%agument)

            ## perturb here for test argumnetation  ## ----
            test_dataset.images = change_images(test_dataset_images,agument)
            #import pdb; pdb.set_trace()
            predictions = predict( net, test_loader )
            all_predictions[a] = predictions

        # add average case ...
        aguments = aguments + ['average']
        predictions = all_predictions.sum(axis=0)/num_augments
        all_predictions[num_augments] = predictions
        log.write('\n')
      

        try:
            os.stat(out_dir +'/submissions/')
        except:
            os.mkdir(out_dir +'/submissions/')

        # apply thresholds and save all
        for a in range(num_augments+1):

            agument = aguments[a]
            predictions = all_predictions[a]

            test_dir = out_dir +'/submissions/'+ agument
            try:
                os.stat(test_dir)
            except:
                os.mkdir(test_dir)

            assert(type(test_loader.sampler)==torch.utils.data.sampler.SequentialSampler)
            np.save(test_dir +'/predictions_train.npy',predictions)

            if a == num_augments:	
                final_predictions[val_index] = predictions





    info = 'oof_train_7pos'
    create_submission(final_predictions, img_id, labels, info)


    pass






def do_submissions():


    out_dir = './output'
    model_file = out_dir +'/snap/final.torch'  

 
    #modify here!
    thresholds=\
        [0.26, 0.23, 0.15, 0.12, 0.28, 0.18, 0.19, 0.19, 0.19, 0.28, 0.08, 0.10, 0.14, 0.16, 0.11, 0.11, 0.04]
    thresholds=np.array(thresholds)
    ## ------------------------------------

    log = Logger()
    log.open(out_dir+'/log.submissions.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('\n')

    ## dataset ----------------------------
    log.write('** dataset setting **\n')
    batch_size    = 48  #128

    test_dataset = KgForestDataset('test-61191',  #'valid-8000',   #'train-simple-road-280',
                                    transform=transforms.Compose([
                                        transforms.Lambda(lambda x: img_to_tensor(x)),
                                    ]),
                                    height=SIZE,width=SIZE,
                                    label_csv='test_v2.csv')
    test_loader  = DataLoader(
                        test_dataset,
                        sampler     = SequentialSampler(test_dataset),  #None,
                        batch_size  = batch_size,
                        drop_last   = False,
                        num_workers = 4,
                        pin_memory  = True)

    height, width , in_channels   = test_dataset.images[0].shape
    log.write('\t(height,width)    = (%d, %d)\n'%(height,width))
    log.write('\tin_channels       = %d\n'%(in_channels))
    log.write('\ttest_dataset.split = %s\n'%(test_dataset.split))
    log.write('\ttest_dataset.num  = %d\n'%(test_dataset.num))
    log.write('\tbatch_size        = %d\n'%batch_size)
    log.write('\n')


    ## net ----------------------------------------
    log.write('** net setting **\n')
    log.write('\tmodel_file = %s\n'%model_file)
    log.write('\n')

    net = torch.load(model_file)
    net.cuda().eval()


    # do testing here ###
    #aguments = ['default']
    aguments = ['default', 'left-right', 'up-down', 'transpose',
                'rotate90', 'rotate180', 'rotate270', ]
    num_augments = len(aguments)
    num_classes  = len(test_dataset.class_names)
    labels_map = test_dataset.class_names
    test_num     = test_dataset.num
    test_dataset_images = test_dataset.images.copy()

    all_predictions = np.zeros((num_augments+1,test_num,num_classes),np.float32)
    for a in range(num_augments):
        agument = aguments[a]
        log.write('** predict @ agument = %s **\n'%agument)

        ## perturb here for test argumnetation  ## ----
        test_dataset.images = change_images(test_dataset_images,agument)
        predictions = predict( net, test_loader )
        all_predictions[a] = predictions

    # add average case ...
    aguments = aguments + ['average']
    predictions = all_predictions.sum(axis=0)/num_augments
    all_predictions[num_augments] = predictions
    log.write('\n')


    try:
        os.stat(out_dir +'/submissions/')
    except:
        os.mkdir(out_dir +'/submissions/')

    # apply thresholds and save all
    for a in range(num_augments+1):

        agument = aguments[a]
        predictions = all_predictions[a]

        thd_value = []
        thd_file = os.path.join(out_dir, 'thresholds', agument, 'best_threshold.txt') 
        
        f = open(thd_file, 'rb');
        lines = f.readlines()
        for l in lines: 
            thd_value.append(np.array(l,np.float32))
        thresholds = np.array(thd_value)        
        print thresholds


        test_dir = out_dir +'/submissions/'+ agument
        try:
            os.stat(test_dir)
        except:
            os.mkdir(test_dir)


        assert(type(test_loader.sampler)==torch.utils.data.sampler.SequentialSampler)
        np.save(test_dir +'/predictions.npy',predictions)
        np.savetxt(test_dir +'/thresholds.txt',thresholds)


        #write_submission_csv(test_dir + '/results.csv', predictions, thresholds )
        sub_file = test_dir + '/results.csv'

 	df_train = pd.read_csv(os.path.join(KAGGLE_DATA_DIR,'test_v2.csv'))        
	files = df_train['image_name']

        predictions_labels = []
        for prediction in predictions:
            label = [labels_map[i] for i, value in enumerate(prediction) if value > thresholds[i]]
    	    predictions_labels.append(label)

        subm = pd.DataFrame()
        subm['image_name'] = files
        subm['tags'] = predictions_labels
        subm.to_csv(sub_file, index=False)

    pass



def create_submission(predictions, test_id, labels, info):
    result1 = pd.DataFrame(predictions, columns=labels)
    result1.loc[:, 'image_name'] = pd.Series(test_id, index=result1.index)
    sub_file = info + '.csv'
    result1.to_csv(sub_file, index=False)



def generate_csv():

    predict_files = [
        './output/resnet50/model0/submissions/average/predictions_test.npy',
        './output/resnet50/model1/submissions/average/predictions_test.npy',
        './output/resnet50/model2/submissions/average/predictions_test.npy',
        './output/resnet50/model3/submissions/average/predictions_test.npy',
        './output/densenet121/model0/submissions/average/predictions_test.npy',
        './output/densenet121/model1/submissions/average/predictions_test.npy',
        './output/densenet121/model2/submissions/average/predictions_test.npy',
        './output/densenet121/model3/submissions/average/predictions_test.npy',
    ]

    df_train = pd.read_csv(os.path.join(KAGGLE_DATA_DIR,'test_v2.csv'))        
    img_id = df_train['image_name']
    labels = CLASS_NAMES
    print labels
    num = len(predict_files)
    for n in range(0,num):
        predictions = np.load(predict_files[n])
        info = 'tst_'+str(n)
        create_submission(predictions, img_id, labels, info)

 





# averaging over many models ####################################################################
def do_find_thresholds1():

    out_dir    ='../standard-7/output'
    thd_dir    ='../standard-7/output/final_thd'


    ## ------------------------------------
    log = Logger()
    log.open(out_dir+'/log.thresholds.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('\n')



    predict_files = [
        '../standard-7/output/final_thd/resnet34/predictions.npy',
        '../standard-7/output/final_thd/resnet50/predictions.npy',
        '../standard-7/output/final_thd/inception_v3/predictions.npy',
        '../standard-7/output/final_thd/resnet101/predictions.npy',
        '../standard-7/output/final_thd/resnet152/predictions.npy',
        '../standard-7/output/final_thd/densenet121/predictions.npy',
        '../standard-7/output/final_thd/densenet169/predictions.npy',
        '../standard-7/output/final_thd/densenet201/predictions.npy',
        '../standard-7/output/final_thd/resnet18/predictions.npy',
        '../standard-7/output/final_thd/densenet121_2/predictions.npy',
        '../standard-7/output/final_thd/densenet169_2/predictions.npy',
        '../standard-7/output/final_thd/resnet34_2/predictions.npy',
        '../standard-7/output/final_thd/resnet50_2/predictions.npy',
        '../standard-7/output/final_thd/densenet121_3/predictions.npy',
        '../standard-7/output/final_thd/densenet169_3/predictions.npy',
        '../standard-7/output/final_thd/resnet34_3/predictions.npy',
        '../standard-7/output/final_thd/resnet50_3/predictions.npy',
    ]
    label_file='../standard-7/output/final_thd/resnet34/labels.npy'

   

    # averaging -----------
    num = len(predict_files)
    predictions = np.load(predict_files[0])
    for n in range(1,num):
        predictions += np.load(predict_files[n])
    predictions = predictions/num

    labels = np.load(label_file)

    test_dir = thd_dir +'/average17'

    try:
        os.stat(test_dir)
    except:
        os.mkdir(test_dir)


    #find threshold --------------------------
    if 1:
        log.write('\tmethod1:\n')

        thresholds, scores = find_f_measure_threshold1(predictions, labels)
        i = np.argmax(scores)
        best_threshold, best_score = thresholds[i], scores[i]

        log.write('\tbest_threshold=%f, best_score=%f\n\n'%(best_threshold, best_score))
        #plot_f_measure_threshold(thresholds, scores)
        #plt.pause(0)

    if 1:
        log.write('\tmethod2:\n')

        seed = best_threshold  #0.21  #best_threshold
        best_thresholds,  best_scores = find_f_measure_threshold2(predictions, labels, num_iters=100, seed=seed)
        #from bruno
        best_thresholds = find_f_measure_threshold2_optim(predictions, labels)


        log.write('\tbest_threshold\n')
        log.write (str(best_thresholds)+'\n')
        log.write('\tbest_scores\n')
        log.write (str(best_scores)+'\n')
        log.write('\n')

    #--------------------------------------------
    np.save(test_dir +'/predictions.npy',predictions)
    np.save(test_dir +'/labels.npy',labels)
    np.savetxt(test_dir +'/best_threshold.txt', np.array(best_thresholds),fmt='%.5f' )
    np.savetxt(test_dir +'/best_scores.txt',np.array(best_scores),fmt='%.5f')

    # pass




#averaging over many models
def do_submission1():

    out_dir ='../standard-7/output'
    sub_dir    ='../standard-7/output/final_sub'

    ## ------------
    log = Logger()
    log.open(out_dir+'/log.submission.txt',mode='a')
    log.write('\n--- [START %s] %s\n\n' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '-' * 64))
    log.write('\n')


    predict_files = [
        '../standard-7/output/final_sub/resnet34/predictions.npy',
        '../standard-7/output/final_sub/resnet50/predictions.npy',
        '../standard-7/output/final_sub/inception_v3/predictions.npy',
        '../standard-7/output/final_sub/resnet101/predictions.npy',
        '../standard-7/output/final_sub/resnet152/predictions.npy',
        '../standard-7/output/final_sub/densenet121/predictions.npy',
        '../standard-7/output/final_sub/densenet169/predictions.npy',
        '../standard-7/output/final_sub/densenet201/predictions.npy',
        '../standard-7/output/final_sub/resnet18/predictions.npy',
        '../standard-7/output/final_sub/densenet121_2/predictions.npy',
        '../standard-7/output/final_sub/densenet169_2/predictions.npy',
        '../standard-7/output/final_sub/resnet34_2/predictions.npy',
        '../standard-7/output/final_sub/resnet50_2/predictions.npy',
        '../standard-7/output/final_sub/densenet121_3/predictions.npy',
        '../standard-7/output/final_sub/densenet169_3/predictions.npy',
        '../standard-7/output/final_sub/resnet34_3/predictions.npy',
        '../standard-7/output/final_sub/resnet50_3/predictions.npy',
   ]

    # averaging -----------
    num=len(predict_files)
    predictions = np.load(predict_files[0])
    for n in range(1,num):
        predictions += np.load(predict_files[n])
    predictions = predictions/num
    # -------------------



################################


#CLASS_NAMES=[
#    'clear',    	 # 0   28k
#    'haze',	         # 1   3k 
#    'partly_cloudy', # 2       7.5k
#    'cloudy',	     # 3       2.5k
#    'primary',	     # 4       37k 
#    'agriculture',   # 5       12k
#    'water',	     # 6       7k 
#    'cultivation',	 # 7   5k 
#    'habitation',	 # 8   4k 
#    'road',	         # 9   8k
#    'slash_burn',	 # 10  0.2k 
#    'conventional_mine', # 11  0.1k 
#    'bare_ground',	 # 12  1k
#    'artisinal_mine',	 # 13  0.3k
#    'blooming',	         # 14  0.3k
#    'selective_logging', # 15  0.3k
#    'blow_down',	 # 16  0.1k
#]


################################



    thd_value = []
    thd_file = os.path.join(out_dir, 'final_thd/average17', 'best_threshold.txt') 
        
    f = open(thd_file, 'rb');
    lines = f.readlines()
    for l in lines: 
        thd_value.append(np.array(l,np.float32))
    thresholds = np.array(thd_value)

    thresholds[10:] = thresholds[10:] - 0.010
    thresholds[:10] = thresholds[:10] - 0.035

 
    print thresholds


    test_dir = sub_dir +'/average17'

    try:
        os.stat(test_dir)
    except:
        os.mkdir(test_dir)

    #write_submission_csv(test_dir + '/results.csv', predictions, thresholds )
    np.save(test_dir +'/predictions.npy',predictions)
    np.savetxt(test_dir +'/thresholds.txt',thresholds)


    sub_file = test_dir + '/results.csv'

    df_train = pd.read_csv(os.path.join(KAGGLE_DATA_DIR,'test_v2.csv'))        
    files = df_train['image_name']
    labels_map = CLASS_NAMES

    predictions_labels = []

    for prediction in predictions:
        label = [labels_map[i] for i, value in enumerate(prediction) if value > thresholds[i]]
        predictions_labels.append(label)

    subm = pd.DataFrame()
    subm['image_name'] = files
    subm['tags'] = predictions_labels
    subm.to_csv(sub_file, index=False)



# main #################################################################
if __name__ == '__main__':
    print( '%s: calling main function ... ' % os.path.basename(__file__))


    ########################for single model################
    #train one model
    #do_training()
    #find the optimized threshold
    #do_find_thresholds()
    #generate submission using the optimized threshold
    #do_submissions_train()
    #do_submissions_test()

    ########################for model ensemble###############
    #print('\n find threshold1')
    #do_find_thresholds1()
    #print('\submission1')
    #do_submission1()
    generate_csv()

    print('\nsucess!')
